/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.Board;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author User
 */
public class Game implements Board {

    Cell[][] board = new Cell[3][3];

    public Game() {
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                Cell cell = new Cell(row, col);
                board[row][col] = cell;
            }
        }
    }
       
    @Override
    public PlayerType getCell(int rowIdx, int colIdx) throws CellException {
        if ( (rowIdx >= 0 && rowIdx < 3) && (colIdx >= 0 && colIdx < 3) ) {
            return board[rowIdx][colIdx].getCellsPlayer();
        } else {
            throw new CellException(rowIdx, colIdx, "The cell is not exists");
        }
    }

    @Override
    public void put(Cell cell) throws CellException {
        int row = cell.getRow();
        int col = cell.getCol();
        PlayerType pt = getCell(row, col);
        //System.out.println(pt);
        if (!(pt.equals(PlayerType.EMPTY))) {
            throw new CellException(row, col, "The cell is not free");
        } else {
            board[row][col].setCellsPlayer(cell.getCellsPlayer());
        }
    }

    @Override
    public boolean hasWon(PlayerType p) {
        if ( p.equals(board[0][0].getCellsPlayer()) && p.equals(board[1][0].getCellsPlayer()) && p.equals(board[2][0].getCellsPlayer()) ) {
            return true;
        } else if ( p.equals(board[0][1].getCellsPlayer()) && p.equals(board[1][1].getCellsPlayer()) && p.equals(board[2][1].getCellsPlayer()) ) {
            return true;
        } else if ( p.equals(board[0][2].getCellsPlayer()) && p.equals(board[1][2].getCellsPlayer()) && p.equals(board[2][2].getCellsPlayer()) ) {
            return true;
        } else if ( p.equals(board[0][0].getCellsPlayer()) && p.equals(board[0][1].getCellsPlayer()) && p.equals(board[0][2].getCellsPlayer()) ) {
            return true;
        } else if ( p.equals(board[1][0].getCellsPlayer()) && p.equals(board[1][1].getCellsPlayer()) && p.equals(board[1][2].getCellsPlayer()) ) {
            return true;
        } else if ( p.equals(board[2][0].getCellsPlayer()) && p.equals(board[2][1].getCellsPlayer()) && p.equals(board[2][2].getCellsPlayer()) ) {
            return true;
        } else if ( p.equals(board[0][0].getCellsPlayer()) && p.equals(board[1][1].getCellsPlayer()) && p.equals(board[2][2].getCellsPlayer()) ) {
            return true;
        } else if ( p.equals(board[0][2].getCellsPlayer()) && p.equals(board[1][1].getCellsPlayer()) && p.equals(board[2][0].getCellsPlayer()) ) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public List<Cell> emptyCells() {
        ArrayList<Cell> list = new ArrayList<>();
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                try {
                    PlayerType pt = getCell(row, col);
                    if (pt.equals(PlayerType.EMPTY)) {
                        list.add(board[row][col]);
                    }
                } catch (CellException a) {   
                }
            }
        }
        return list;
    }
    
}
