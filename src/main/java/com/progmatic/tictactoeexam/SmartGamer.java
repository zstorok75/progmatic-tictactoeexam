/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.AbstractPlayer;
import com.progmatic.tictactoeexam.interfaces.Board;

/**
 *
 * @author Zsozso
 */
public class SmartGamer extends AbstractPlayer {
    
    

    public SmartGamer(PlayerType p) {
        super(p);
    }

    @Override
    public Cell nextMove(Board b) {
        Cell cell = null;
        int empty = 0;
        int my = 0;
        try {
            // oszloponkent nezem at
            for (int col = 0; col < 3; col++) {
                empty = 0;
                my = 0;
                for (int row = 0; row < 3; row++) {
                    if ( b.getCell(row, col).equals(myType) ) {
                        my++;
                    } else if (b.getCell(row, col).equals(PlayerType.EMPTY)) {
                        empty++;
                    }
                }
                if (my == 2 && empty == 1) {
                    if ( (b.getCell(0, col).equals(myType)) && (b.getCell(1, col).equals(myType)) ) {
                        cell = new Cell(2,col);
                    } else if( (b.getCell(0, col).equals(myType)) && (b.getCell(2, col).equals(myType)) ) {
                        cell = new Cell(1,col);
                    } else {
                        cell = new Cell(0,col);
                    }
                    cell.setCellsPlayer(myType);
                    return cell;
                }
            }
            //soronkent nezem at
            for (int row = 0; row < 3; row++) {
                empty = 0;
                my = 0;
                for (int col = 0; col < 3; col++) {
                    if ( b.getCell(row, col).equals(myType) ) {
                        my++; 
                    } else if (b.getCell(row, col).equals(PlayerType.EMPTY)) {
                        empty++;
                    }
                }
                if (my == 2 && empty == 1) {
                    if ( (b.getCell(row, 0).equals(myType)) && (b.getCell(row, 1).equals(myType)) ) {
                        cell = new Cell(row,2);
                    } else if( (b.getCell(row, 0).equals(myType)) && (b.getCell(row, 2).equals(myType)) ) {
                        cell = new Cell(row,1);
                    } else {
                        cell = new Cell(row,0);
                    }
                    cell.setCellsPlayer(myType);
                    return cell;
                }
            }
            // bal felsotol jobb also atlot nezem
            for (int i = 0; i < 3; i++) {
                if ( b.getCell(i, i).equals(myType) ) {
                    my++; 
                } else if (b.getCell(i, i).equals(PlayerType.EMPTY)) {
                    empty++;
                }
            }
            if (my == 2 && empty == 1) {
                if ( (b.getCell(0, 0).equals(myType)) && (b.getCell(1, 1).equals(myType)) ) {
                    cell = new Cell(2,2);
                } else if( (b.getCell(0, 0).equals(myType)) && (b.getCell(2, 2).equals(myType)) ) {
                    cell = new Cell(1,1);
                } else {
                    cell = new Cell(0,0);
                }
                cell.setCellsPlayer(myType);
                return cell;
            }
            // jobb felsotol bal also atlot nezem
            for (int i = 0; i < 3; i++) {
                if ( b.getCell(2-i, i).equals(myType) ) {
                    my++; 
                } else if (b.getCell(2-i, i).equals(PlayerType.EMPTY)) {
                    empty++;
                }
            }
            if (my == 2 && empty == 1) {
                if ( (b.getCell(2, 0).equals(myType)) && (b.getCell(1, 1).equals(myType)) ) {
                    cell = new Cell(0,2);
                } else if( (b.getCell(2, 0).equals(myType)) && (b.getCell(0, 2).equals(myType)) ) {
                    cell = new Cell(1,1);
                } else {
                    cell = new Cell(2,0);
                }
                cell.setCellsPlayer(myType);
                return cell;
            }
            for (int row = 0; row < 3; row++) {
                for (int col = 0; col < 3; col++) {
                    PlayerType playerInCell = b.getCell(row, col);
                    if (playerInCell.equals(PlayerType.EMPTY)) {
                        cell = new Cell(row, col);
                        cell.setCellsPlayer(myType);
                        return cell; 
                    }
                }
            }

        } catch (CellException a) {
            
        }
        return cell;
    }
    
}
