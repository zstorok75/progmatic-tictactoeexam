/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.AbstractPlayer;
import com.progmatic.tictactoeexam.interfaces.Board;

/**
 *
 * @author User
 */
public class SimpleGamer extends AbstractPlayer {

    public SimpleGamer(PlayerType p) {
        super(p);
    }

    @Override
    public Cell nextMove(Board b) {
        Cell cell = null;
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                try {
                    PlayerType playerInCell = b.getCell(row, col);
                    if (playerInCell.equals(PlayerType.EMPTY)) {
                        cell = new Cell(row, col);
                        cell.setCellsPlayer(myType);
                        return cell; 
                    }
                } catch (CellException a) {
                }
            }
        }
        return cell;
    }
    
}
